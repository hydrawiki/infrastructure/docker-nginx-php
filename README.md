# Hydra Base Docker Image
This docker image creates the base for docker image to support the hydra platform.

The Image add support for: 
php-fpm
nginx (openresty)
luasandbox
nodejs
ploticus
texvc
supervisor
git
tidy
cloudwatch agent
logrotate
syslog-ng

