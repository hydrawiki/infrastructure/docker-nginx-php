# The actual php image
FROM php:7.3.12-fpm

ENV CFLAGS -O3
ENV CXXFLAGS -O3

ARG PECL_APCU_VERSION='5.1.18'
ARG PECL_MEMCACHED_VERSION='3.1.5'

RUN apt-get update && apt-get install -y \
    libbz2-dev libzstd-dev \
    libfreetype6-dev libjpeg62-turbo-dev libpng-dev libz-dev libxpm-dev libwebp-dev fonts-freefont-ttf \
    libicu-dev \
    libpq-dev \
    libreadline-dev libedit-dev \
    libxml2-dev \
    libmemcached-dev \
    libmagickwand-dev \
    gnupg wget \
    webp \
 && docker-php-ext-configure gd \
    --with-freetype-dir=/usr/include/ \
    --with-jpeg-dir=/usr/include/ \
    --with-png-dir=/usr/include/ \
    --with-webp-dir=/usr/include/ \
    --with-xpm-dir=/usr/include/ \
 && docker-php-ext-install gd opcache bcmath calendar exif intl pdo_mysql sockets \
    bz2 mysqli pgsql readline soap \
 && pecl install apcu-$PECL_APCU_VERSION \
 && docker-php-ext-enable apcu \
 && pecl install igbinary \
 && docker-php-ext-enable igbinary \
 && pecl download memcached-$PECL_MEMCACHED_VERSION \
 && tar -xzf memcached-$PECL_MEMCACHED_VERSION.tgz \
 && rm memcached-$PECL_MEMCACHED_VERSION.tgz \
 && (cd memcached-$PECL_MEMCACHED_VERSION && phpize && ./configure --enable-memcached-igbinary && make && make install) \
 && rm -r memcached-$PECL_MEMCACHED_VERSION \
 && docker-php-ext-enable memcached \
 && yes | pecl install redis \
 && pecl install imagick oauth < /dev/null \
 && docker-php-ext-enable redis imagick oauth \
 && echo 'deb http://apt.newrelic.com/debian/ newrelic non-free' | tee /etc/apt/sources.list.d/newrelic.list \
 && wget -O- https://download.newrelic.com/548C16BF.gpg | apt-key add - \
 && apt-get update && apt-get install -y newrelic-php5 \
 && cp /usr/lib/newrelic-php5/agent/x64/newrelic-20180731.so /usr/local/lib/php/extensions/no-debug-non-zts-20180731/newrelic.so \
 && docker-php-ext-enable newrelic \
 && rm -rf /var/lib/apt/lists/*

# set user and group ids for www-data
RUN usermod -u 498 www-data \
 && groupmod -g 496 www-data

# build and install luasandbox extension (for Scribunto)
# also get graphviz and postfix in
RUN DEBIAN_FRONTEND=noninteractive apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    graphviz postfix libsasl2-modules mscgen \
    lua5.1 lua5.1-dev \
 && cd /tmp \
 && wget -O luasandbox.tar.gz https://github.com/wikimedia/mediawiki-php-luasandbox/archive/master.tar.gz \
 && tar -zxvf luasandbox.tar.gz \
 && rm luasandbox.tar.gz \
 && (cd mediawiki-php-luasandbox-master && phpize && ./configure && make && make install) \
 && rm -r mediawiki-php-luasandbox-master \
 && docker-php-ext-enable luasandbox \
 && rm -rf /var/lib/apt/lists/*

# download and install nodejs 12.x
ENV NODE_VERSION 12.13.1

RUN curl -SLO "https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-x64.tar.xz" \
 && tar -xJf "node-v$NODE_VERSION-linux-x64.tar.xz" -C /usr/local --strip-components=1 --no-same-owner \
 && rm "node-v$NODE_VERSION-linux-x64.tar.xz" \
 && ln -s /usr/local/bin/node /usr/local/bin/nodejs

# download and build ploticus
RUN wget https://downloads.sourceforge.net/project/ploticus/ploticus/2.42/ploticus242_src.tar.gz \
 && tar -xzf ploticus242_src.tar.gz \
 && rm ploticus242_src.tar.gz \
 && cd ploticus242/src \
 && make \
 && cp pl /usr/local/bin/ploticus \
 && cd ../.. \
 && rm -r ploticus242

# download and build texvc for Math, and install texlive (this is a gigantic binary distribution)
RUN apt-get update && apt-get install -y --no-install-recommends \
    dvipng ocaml texlive-fonts-recommended texlive-lang-greek texlive-latex-recommended texlive-latex-extra imagemagick \
 && wget https://extdist.wmflabs.org/dist/extensions/Math-REL1_31-a1263db.tar.gz \
 && tar -xzf Math-REL1_31-a1263db.tar.gz \
 && rm Math-REL1_31-a1263db.tar.gz \
 && cd Math \
 && make \
 && cp math/texvc /usr/bin/texvc \
 && cd .. \
 && rm -r Math \
 && apt-get remove -y --auto-remove ocaml \
 && apt-get purge -y $(dpkg --list |grep '^rc' |awk '{print $2}') \
 && apt-get autoremove -y \
 && rm -rf /var/lib/apt/lists/*

# Install latest supervisor and install git
# Give www-data a shell
# Install pcntl (used by SyncService)
# Install nscd (cache DNS queries)
RUN apt-get update && apt-get install -y python-pip git nscd default-mysql-client redis-tools vim tree jq \
 && pip install supervisor awscli \
 && mkdir -p /home/hydra/public_html \
 && chown www-data:www-data /home/hydra/public_html \
 && usermod -s /bin/bash -d /home/hydra/public_html www-data \
 && docker-php-ext-install pcntl \
 && rm package.xml \
 && rm -rf /var/lib/apt/lists/* \
 # configure vim to not mess with indents
 && printf 'set paste\n' > /etc/vim/vimrc.local
WORKDIR "/home/hydra/public_html"

# download and build tidy (/usr/local/bin/tidy)
RUN apt-get update && apt-get install -y cmake \
 && git clone https://github.com/htacg/tidy-html5.git \
 && cd tidy-html5 \
 && cmake . \
 && make \
 && make install \
 && cd .. \
 && rm -r tidy-html5 \
 && rm -rf /var/lib/apt/lists/*

# install openresty
ARG RESTY_DEB_VERSION="=1.15.8.2-1~buster1"
RUN DEBIAN_FRONTEND=noninteractive apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        ca-certificates \
        gettext-base \
        gnupg2 \
        lsb-release \
        software-properties-common \
        wget \
    && wget -qO /tmp/pubkey.gpg https://openresty.org/package/pubkey.gpg \
    && DEBIAN_FRONTEND=noninteractive apt-key add /tmp/pubkey.gpg \
    && rm /tmp/pubkey.gpg \
    && DEBIAN_FRONTEND=noninteractive add-apt-repository -y "deb http://openresty.org/package/debian $(lsb_release -sc) openresty" \
    && DEBIAN_FRONTEND=noninteractive apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        openresty${RESTY_DEB_VERSION} \
    && DEBIAN_FRONTEND=noninteractive apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/*

# install cloudwatch agent, logrotate, and syslog-ng
RUN wget https://s3.amazonaws.com/amazoncloudwatch-agent/debian/amd64/latest/amazon-cloudwatch-agent.deb \
 && dpkg -i amazon-cloudwatch-agent.deb \
 && rm amazon-cloudwatch-agent.deb \
 # use a new folder for logs to avoid potential conflict with old versions using the Docker bind-mounted /usr/local/var/log
 && mkdir -p /usr/local/hydra/logs \
 # install logrotate and cron
 && DEBIAN_FRONTEND=noninteractive apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends logrotate cron syslog-ng \
 && DEBIAN_FRONTEND=noninteractive apt-get autoremove -y \
 && rm -rf /var/lib/apt/lists/* \
 # run logrotate hourly instead of the default daily
 && mv /etc/cron.daily/logrotate /etc/cron.hourly/logrotate \
 # make sure cron can't mail
 && sed -i '/PATH=/a MAILTO=""' /etc/crontab

# Add additional binaries into PATH for convenience
ENV PATH="$PATH:/usr/local/openresty/luajit/bin:/usr/local/openresty/nginx/sbin:/usr/local/openresty/bin"
